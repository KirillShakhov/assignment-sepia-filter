#include "../include/image.h"

bool image_malloc(struct image *img){
    img->data = malloc(sizeof(struct pixel) * img->width * img->height);
    return img->data == NULL;
}

void image_free(struct image *img){
    if (img != NULL && img->data != NULL) free(img->data);
}
