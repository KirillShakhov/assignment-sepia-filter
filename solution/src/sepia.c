#include "image.h"
#include <inttypes.h>
#include <stdlib.h>

static unsigned char sat( uint64_t x) {
    if (x < 256) return x; return 255;
}

struct image sepia_filter(struct image img) {
    struct pixel *pixels = malloc(sizeof(struct pixel) * img.height * img.width);
    struct image result_image;
    result_image.width = img.width;
    result_image.height = img.height;
    result_image.data = pixels;

    for (uint64_t y = 0; y < img.height; y++) {
        for (uint64_t x = 0; x < img.width; x++) {
            static const float c[3][3] =  {
                    { .393f, .769f, .189f },
                    { .349f, .686f, .168f },
                    { .272f, .543f, .131f } };
            result_image.data[img.width * y + x].r = sat(
                    (float)img.data[img.width * y + x].r * c[0][0] + (float)img.data[img.width * y + x].g * c[0][1] + (float)img.data[img.width * y + x].b *  c[0][2]
            );
            result_image.data[img.width * y + x].g = sat(
                    (float)img.data[img.width * y + x].r *  c[1][0] + (float)img.data[img.width * y + x].g *  c[1][1] + (float)img.data[img.width * y + x].b  *  c[1][2]
            );
            result_image.data[img.width * y + x].b = sat(
                    (float)img.data[img.width * y + x].r *  c[2][0] + (float)img.data[img.width * y + x].g *  c[2][1] + (float)img.data[img.width * y + x].b *  c[2][2]
            );
        }
    }
    return result_image;
}
