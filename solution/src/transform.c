#include "../include/transform.h"

#ifdef __MACH__
#include <stdlib.h>
#else
#include <malloc.h>
#endif

struct image rotate_left_90(struct image img){
    struct pixel *pixels = malloc(sizeof(struct pixel) * img.height * img.width);

    for (size_t i = 0; i < img.height; i++ ) {
        for (size_t j = 0; j < img.width; j++) {
            *(pixels + j * img.height + (img.height - 1 - i)) = *(img.data + i * img.width + j);
        }
    }
    struct image img1;
    img1.width = img.height;
    img1.height = img.width;
    img1.data = pixels;
    return img1;
}

struct image rotate_left_180(struct image img){
    struct image img1 = rotate_left_90(img);
    img1 = rotate_left_90(img1);
    return img1;
}

struct image rotate_left_270(struct image img){
    struct image img1 = rotate_left_90(img);
    img1 = rotate_left_90(img1);
    img1 = rotate_left_90(img1);
    return img1;
}

struct image rotate_left_360(struct image img){
    return img;
}

struct image rotate_left_0(struct image img){
    return img;
}

struct image rotate_right_90(struct image img){
    struct image img1 = rotate_left_90(img);
    img1 = rotate_left_90(img1);
    img1 = rotate_left_90(img1);
    return img1;
}

struct image rotate_right_180(struct image img){
    struct image img1 = rotate_left_90(img);
    img1 = rotate_left_90(img1);
    return img1;
}

struct image rotate_right_270(struct image img){
    return rotate_left_90(img);
}

struct image rotate_right_360(struct image img){
    return img;
}
struct image rotate_right_0(struct image img){
    return img;
}
