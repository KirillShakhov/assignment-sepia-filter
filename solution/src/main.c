#include "../include/bmp.h"
#include "../include/io_file.h"
#include "../include/transform.h"
#include <string.h>

int main(int argc, char **argv) {
    char *path_input_file = NULL, *path_output_file = NULL, *rotation = "left", *degree = "90";
    struct image img;
    struct image new_image;
    if (argc < 3) {
        fprintf(stderr, "You have specified the wrong number of arguments\n");
        return 0;
    } else if (argc == 5) {
        path_input_file = argv[1];
        path_output_file = argv[2];
        rotation = argv[3];
        degree = argv[4];
    } else if (argc == 4) {
        path_input_file = argv[1];
        path_output_file = argv[2];
        rotation = argv[3];
    } else {
    	path_input_file = argv[1];
    	path_output_file = argv[2];
    }
    FILE *input_file = NULL, *output_file = NULL;
    bool in = open_file(&input_file, path_input_file, "rb");
    if (in) {
        fprintf(stderr, "Unable to read input file\n");
        return 0;
    }
    bool out = open_file(&output_file, path_output_file, "wb");
    if (out) {
        fprintf(stderr, "Unable to create file\n");
        return 0;
    }
    if (from_bmp(input_file, &img)){
        fprintf(stderr, "Unable to convert this file\n");
        return 0;
    } else{
    	fprintf(stdout, "Your file has been successfully converted\n");
    }
    if(strcmp(rotation, "right") == 0){
        fprintf(stdout, "Rotated %s %s degrees\n", rotation, degree);
        if(strcmp(degree, "0") == 0){
            new_image = rotate_right_0(img);
        }
        else if(strcmp(degree, "90") == 0){
            fprintf(stdout, "90\n");
            new_image = rotate_right_90(img);
        }
        else if(strcmp(degree, "180") == 0){
            new_image = rotate_right_180(img);
        }
        else if(strcmp(degree, "270") == 0){
            new_image = rotate_right_270(img);
        }
        else if(strcmp(degree, "360") == 0){
            new_image = rotate_right_360(img);
        }
        else{
            fprintf(stderr, "Function not found\n");
            return 0;
        }
    }
    else if(strcmp(rotation, "left") == 0){
        fprintf(stdout, "Rotated %s %s degrees\n", rotation, degree);
        if(strcmp(degree, "0") == 0){
            new_image = rotate_left_0(img);
        }
        else if(strcmp(degree, "90") == 0){
            new_image = rotate_left_90(img);
        }
        else if(strcmp(degree, "180") == 0){
            new_image = rotate_left_180(img);
        }
        else if(strcmp(degree, "270") == 0){
            new_image = rotate_left_270(img);
        }
        else if(strcmp(degree, "360") == 0){
            new_image = rotate_left_360(img);
        }
        else{
            fprintf(stderr, "Function not found\n");
            return 0;
        }
    }
    else if(strcmp(rotation, "sepia") == 0){
        new_image = sepia_filter(img);
        fprintf(stdout, "Sepia filter\n");
    }
    else{
        fprintf(stderr, "Function not found\n");
    }
    if (to_bmp(output_file, &new_image)){
        fprintf(stderr, "Unable to convert image\n");
        return 0;
    }else {
    	fprintf(stdout, "The picture has been successfully converted\n");
    }

    in = close_file(&input_file);
    out = close_file(&output_file);
    if (in){
        fprintf(stderr, "Unable to input close file\n");
        return 0;
    }
    if (out){
        fprintf(stderr, "Unable to output close file\n");
        return 0;
    }
    image_free(&img);
    image_free(&new_image);
    return 0;
}
