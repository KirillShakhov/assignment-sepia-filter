#include "../include/bmp.h"

#ifdef __MACH__

#include <stdlib.h>

#else
#include <malloc.h>
#endif

struct __attribute__((packed)) bmp_header {
    uint16_t signature;
    uint32_t filesize;
    uint32_t reserved;
    uint32_t data_offset;
    uint32_t size;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bit_count;
    uint32_t compression;
    uint32_t image_size;
    uint32_t x_pixels_per_m;
    uint32_t y_pixels_per_m;
    uint32_t colors_used;
    uint32_t colors_important;
};

static uint32_t SIGNATURE = 19778;
static uint32_t RESERVED = 0;
static uint32_t HEADER_SIZE = 40;
static uint16_t PLANES = 1;
static uint32_t COMPRESSION = 0;
static uint32_t PIXEL_PER_M = 2834;
static uint32_t COLORS_USED = 0;
static uint32_t COLORS_IMPORTANT = 0;
static size_t BIT_COUNT = 24;

static enum read_status read_bmp_header(FILE *file, struct bmp_header *header);

static enum read_status read_pixels(FILE *file_in, struct image *img);

static struct bmp_header create_header(struct image const *img);

static size_t calculate_padding(size_t width);

enum read_status from_bmp(FILE *file_in, struct image *img) {
    if (!file_in) {
        return READ_NULL;
    }
    if (!img) {
        return READ_NULL;
    }
    struct bmp_header header = {0};
    enum read_status header_status = read_bmp_header(file_in, &header);
    if (header_status) {
        return header_status;
    }
    img->height = header.height;
    img->width = header.width;
    if (fseek(file_in, header.data_offset, SEEK_SET) != 0) {
        return READ_INVALID_HEADER;
    }
    return read_pixels(file_in, img);
}

enum write_status to_bmp(FILE *file_out, struct image *img) {
    struct bmp_header header = create_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, file_out) < 1) {
        fclose(file_out);
        return WRITE_ERROR;
    }
    const size_t padding = (4 - (img->width * 3 % 4)) % 4;
    uint8_t *line_padding = malloc(padding);

    for (size_t i = 0; i < padding; i++) {
        *(line_padding + i) = 0;
    }
    if (img->data != NULL) {
        for (size_t i = 0; i < img->height; i++) {
            fwrite(img->data + i * img->width, img->width * sizeof(struct pixel), 1, file_out);
            fwrite(line_padding, padding, 1, file_out);
        }
    }
    free(line_padding);
    fclose(file_out);
    return WRITE_OK;

}


static enum read_status read_bmp_header(FILE *file, struct bmp_header *header) {
    if (fseek(file, 0, SEEK_END) != 0) return READ_INVALID_HEADER;
    size_t f_size = ftell(file);
    if (f_size < sizeof(struct bmp_header)) return READ_INVALID_HEADER;
    rewind(file);
    size_t res = fread(header, sizeof(struct bmp_header), 1, file);
    if (res != 1) return READ_INVALID_HEADER;
    return READ_OK;
}

static enum read_status read_pixels(FILE *file_in, struct image *img) {
    if (!image_malloc(img)) {
        size_t padding = calculate_padding(img->width);
        for (size_t i = 0; i < img->height; i++) {
            size_t res = fread(img->data + i * (img->width), (size_t) (img->width) * sizeof(struct pixel), 1, file_in);
            if (res != 1) {
                return READ_NULL;
            }
            if (fseek(file_in, padding, SEEK_CUR) != 0) {
                return READ_NULL;
            }
        }
        return READ_OK;
    } else {
        return READ_ERROR;
    }
}

static struct bmp_header create_header(struct image const *img) {
    return (struct bmp_header) {
            .signature = SIGNATURE,
            .image_size = (img->width * sizeof(struct pixel) + calculate_padding(img->width)) * img->height,
            .filesize = (uint64_t)(sizeof(struct bmp_header)
                        + ((sizeof(struct pixel) * img->width + calculate_padding(img->width)) * img->height)),
            .reserved = RESERVED,
            .data_offset = sizeof(struct bmp_header),
            .size = HEADER_SIZE,
            .width = img->width,
            .height = img->height,
            .planes = PLANES,
            .bit_count = BIT_COUNT,
            .compression = COMPRESSION,
            .x_pixels_per_m = PIXEL_PER_M,
            .y_pixels_per_m = PIXEL_PER_M,
            .colors_used = COLORS_USED,
            .colors_important = COLORS_IMPORTANT,
    };
}

static size_t calculate_padding(size_t width) {
    size_t padding = 4 - width * sizeof(struct pixel) % 4;
    if (padding != 4) return padding;
    padding = 0;
    return padding;
}
