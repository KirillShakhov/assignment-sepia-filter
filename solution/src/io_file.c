#include "../include/io_file.h"

bool open_file(FILE **file, const char *name, const char *mode) {
    if (file != NULL) {
        *file = fopen(name, mode);
        return *file == NULL;
    } else {
        return false;
    }
}

bool close_file(FILE **file) {
    if (*file) return false;
    return fclose(*file);
}
