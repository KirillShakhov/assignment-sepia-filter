#ifndef LAB3_IMAGE_H
#define LAB3_IMAGE_H
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#pragma pack(push, 1)
struct image {
    uint64_t width, height;
    struct pixel *data;
};
#pragma pack(pop)
struct pixel {
    uint8_t b, g, r;
};

bool image_malloc(struct image *img);
void image_free(struct image *img);

#endif
