#ifndef LAB3_BMP_H
#define LAB3_BMP_H

#include "../include/image.h"
#include  <stdint.h>
#include <stdio.h>



/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_NULL,
    READ_ERROR
};

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_NULL,
    WRITE_ERROR
};


enum write_status to_bmp(FILE *file_out, struct image *img);

enum read_status from_bmp(FILE *file_in, struct image *img);


#endif
