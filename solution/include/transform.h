#ifndef LAB3_TRANSFORM_H
#define LAB3_TRANSFORM_H

#include "image.h"

struct image rotate_left_90(struct image img);
struct image rotate_left_180(struct image img);
struct image rotate_left_270(struct image img);
struct image rotate_left_360(struct image img);
struct image rotate_left_0(struct image img);
struct image rotate_right_90(struct image img);
struct image rotate_right_180(struct image img);
struct image rotate_right_270(struct image img);
struct image rotate_right_360(struct image img);
struct image rotate_right_0(struct image img);

struct image sepia_filter(struct image img);

#endif
